package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    // Test if algorithm for testing whether a box is complete is wrong
    @Test
    public void completeBoxTest() {
        logger.info("Test to see if completion algorithm is working");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4,3,2);
        // draw a complete square
        grid.drawHorizontal(1,1,1);
        grid.drawHorizontal(1,2,2);
        grid.drawVertical(1,1,1);
        assertTrue(grid.drawVertical(2,1,1));
    }

    // Test that DotsAndBoxesGrid currently allows drawing a line on an existing line
    @Test
    public void gridLineException() {
        logger.info("Should not be able to draw a line over an already existing line");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4,2,2);
        grid.drawHorizontal(1,1,1);
        // assert error here
        assertThrows(RuntimeException.class, () -> grid.drawHorizontal(1,1,2), "Line was drawn over another");

    }
}
